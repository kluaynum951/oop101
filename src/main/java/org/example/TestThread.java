package org.example;

import static java.lang.Thread.sleep;

public class TestThread {
    public static void main(String[] args) {
        Counter counter1 = new Counter();
        counter1.start();
        Counter counter2 = new Counter();
        counter2.start();
        Thread counter3 = new Thread(new Counter2());
        counter3.start();

    }
}

class Counter extends Thread {
    @Override
    public void run() {
        System.out.println("Start counting 1");
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

class Counter2 implements Runnable {
    @Override
    public void run() {
        System.out.println("Start counting 2");
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
