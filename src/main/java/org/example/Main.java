package org.example;

import behavior.MagicAttack;
import behavior.PhysicalAttack;
import character.AttackType;
import character.monster.FlyableMonster;
import character.monster.Monster;
import character.monster.SwimMonster;
import character.player.Gender;
import character.player.Player;
import item.Item;
import item.Potion;
import item.Sword;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");
//        Player player1 = new Player("banana");
//        System.out.println(player1);
//        player1.setName("banana222222");
//        System.out.println(player1);
//        player1.setName("banana22");
//        System.out.println(player1.getName());
//        player1.setAttackScore(20);
//        player1.setLiftScore(100);
//        player1.setGender(Gender.MALE);
//        System.out.println(player1);
//        Player player2 = new Player("James",Gender.MALE,100);
//        System.out.println(player2);
//        Player player3 = new Player(100);

//        System.out.println("Create Player and monster");
//        Player player4 = new Player("banana12345", Gender.MALE, 100);
//        Monster monster1 = new Monster("slime", 50, 5, 10);
//        System.out.println(player4);
//        System.out.println(monster1);

//        System.out.println("Player attack monster");
//        monster1.gotAttack(player4.attack());
//        System.out.println("Lift score of player : " + player4.getLiftScore());
//        System.out.println("Lift score of monster : " + monster1.getLiftScore());
//        System.out.println("Monster attack player");
//        player4.gotAttack(monster1.attack());
//        System.out.println("Lift score of player : " + player4.getLiftScore());
//        System.out.println("Lift score of monster : " + monster1.getLiftScore());

//        for (int i = 0; ; i++) {
//            System.out.println("----------- " + (i + 1) + " -----------");
//            System.out.println("Player attack monster");
//            monster1.gotAttack(player4.attack());
//            System.out.println("Lift score of player : " + player4.getLiftScore());
//            System.out.println("Lift score of monster : " + monster1.getLiftScore());
//            if (monster1.getLiftScore() <= 0) {
//                System.out.println("-------------------------");
//                System.out.println("!!!!Monster died!!!!");
//                break;
//            }
//            System.out.println("Monster attack player");
//            player4.gotAttack(monster1.attack());
//            System.out.println("Lift score of player : " + player4.getLiftScore());
//            System.out.println("Lift score of monster : " + monster1.getLiftScore());
//            if (player4.getLiftScore() <= 0) {
//                System.out.println("-------------------------");
//                System.out.println("!!!!Player died!!!!");
//                break;
//            }
//        }
//        FlyableMonster flyableMonster = new FlyableMonster("slime", 100, 20, 10);
//        flyableMonster.fly();
//
//        SwimMonster swimMonster = new SwimMonster("fish", 100, 10, 5);
//        swimMonster.swim();
//
//        flyableMonster.attack();
//
//        Potion potion = new Potion("lift potion", 10);
//        potion.beUse();
//        System.out.println(potion.getName());
//        System.out.println(potion.getType());
//        System.out.println(potion.getPrice());
//        potion.setName("New name");
//        potion.setType("New type");
//        potion.setPrice(20);
//        System.out.println(potion.getName());
//        System.out.println(potion.getType());
//        System.out.println(potion.getPrice());
//
//        Player player = new Player("Banana", Gender.MALE, 100);
//        player.createBackpack(10);
//        player.checkBackpack();
//        Sword sword = new Sword("fish", 1000, 99);
//        Sword sword2 = new Sword("fish*1", 1000, 99);
//        Potion potion = new Potion("HP potion",200);
//        player.addItemToBackpack(sword);
//        player.addItemToBackpack(sword2);
//        player.checkBackpack();
//        Item pickItemByIndex = player.pickItemFromBackpack(0);
//        System.out.println(pickItemByIndex != null ? "Item at pick : " + pickItemByIndex : "Item at pick : " + "Can't pick item!!");
//        player.checkBackpack();
//        Item pickItemByItem = player.pickItemFromBackpack(sword2);
//        System.out.println(pickItemByItem != null ? "Item at pick : " + pickItemByItem : "Item at pick : " + "Can't pick item!!");
//        player.checkBackpack();
//        Item pickPotion = player.pickItemFromBackpack(potion);
//        System.out.println(pickPotion != null ? "Item at pick : " + pickPotion : "Item at pick : " + "Can't pick item!!");
//        player.checkBackpack();

//        เรื่อง interface
        Player player = new Player("Player1",Gender.MALE,100);
        MagicAttack magicAttack = new MagicAttack();
        PhysicalAttack physicalAttack = new PhysicalAttack();
        player.addTypeAttack(AttackType.PHYSICAL_ATTACK,physicalAttack);
        player.attack(AttackType.PHYSICAL_ATTACK);
        Monster monster = new Monster("Slime",100,5);
        monster.addTypeAttack(AttackType.PHYSICAL_ATTACK,physicalAttack);
        monster.attack(AttackType.PHYSICAL_ATTACK);
        player.addTypeAttack(AttackType.MAGIC_ATTACK,magicAttack);
        player.attack(AttackType.MAGIC_ATTACK);
        player.checkTypeAttack();
        player.removeTypeAttack(AttackType.MAGIC_ATTACK);
        player.checkTypeAttack();
    }
}