package org.example;

public class TestException {
    public static void main(String[] args) {
        String hello = "222";
        try {
            int helloInt = Integer.parseInt(hello);
            System.out.println(helloInt);
            TestException test = new TestException();
            test.cellException();
        } catch (InvalidInputException i){
            System.out.println(i);
        } catch (Exception e){
            System.out.println(e);
        } finally {
            System.out.println("Finally");
        }
    }

    public void cellException() throws InvalidInputException {
        throw new InvalidInputException("Something wrong");
    }
}
