package behavior;

public class MagicAttack implements Attackable {
    @Override
    public void attack() {
        System.out.println(" Magic attack!!!");
    }
}
