package behavior;

public class PhysicalAttack implements Attackable{
    @Override
    public void attack() {
        System.out.println(" Physical attack!!");
    }
}
