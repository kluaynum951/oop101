package behavior;

import character.player.Player;

public interface Attackable {
    void attack();
}
