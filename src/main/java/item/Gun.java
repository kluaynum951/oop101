package item;

public class Gun extends Weapon {
    public Gun(String name, double price, int attackPoint) {
        this.name = name;
        this.type = "gun";
        this.price = price;
        this.attackPoint = attackPoint;
        this.upgradeLevel = 0;
    }

    @Override
    public void beUse() {
        System.out.println("Use gun!!!");
    }

    @Override
    public void upgrade() {
        System.out.println("Upgrade gun!!!");
    }
}
