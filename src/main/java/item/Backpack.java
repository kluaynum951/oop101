package item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Backpack {
    private int numberOfSlot;
    //    private Item[] itemList;
    private List<Item> itemList = new ArrayList<>();

    public Backpack(int numberOfSlot) {
//        this.itemList = new Item[numberOfSlot];
        this.numberOfSlot = numberOfSlot;
        Potion potion = new Potion("HP potion",200);
//        itemList.add(potion);
        this.addItem(potion);
    }

    public void addItem(Item item) {
        if (itemList.size() >= numberOfSlot) {
            System.out.println("Backpack have " + numberOfSlot + " slot");
        } else {
            itemList.add(item);
            System.out.println("Items have been stored in the backpack!!");
        }
    }

    public Item pickItem(int slotIndex) {
        if (slotIndex < itemList.size()) {
            return itemList.remove(slotIndex);
//            Item itemToPick = itemList.get(slotIndex);
//            if (itemToPick != null) {
//                itemList.remove(slotIndex);
//                return itemToPick;
//            } else {
//                System.out.println("This slot have no item!!");
//                return null;
//            }
        } else {
            System.out.println("Index slot out of scope!!");
            return null;
        }
    }

    public Item pickItem(Item item) {
        int indexOfItem = itemList.indexOf(item);
        if (indexOfItem >= 0) {
            return itemList.remove(indexOfItem);
        } else {
            System.out.println("Backpack have not " + item.getName());
            return null;
        }
    }

    public void addNumberOfSlot() {

    }

    public void printItemList() {
        System.out.println("item list : " + itemList.toString());
    }
}
