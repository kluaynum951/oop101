package item;

public class Sword extends Weapon {
    public Sword(String name, double price, int attackPoint) {
        this.name = name;
        this.type = "sword";
        this.price = price;
        this.attackPoint = attackPoint;
        this.upgradeLevel = 0;
    }

    @Override
    public void beUse() {
        System.out.println("Use sword!!!");
    }

    @Override
    public void upgrade() {
        System.out.println("Upgrade sword!!!");
    }
}
