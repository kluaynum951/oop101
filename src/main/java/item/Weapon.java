package item;

public abstract class Weapon extends Item{
    protected int attackPoint;
    protected int upgradeLevel;
    public abstract void upgrade();
}
