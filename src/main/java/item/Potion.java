package item;

public class Potion extends Item {
    public Potion(String name, double price) {
        this.name = name;
        this.type = "potion";
        this.price = price;
    }

    @Override
    public void beUse() {
        System.out.println("Use Potion!!!");
    }

//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getName() {
//        return this.name;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getType() {
//        return this.type;
//    }
//
//    public void setPrice(double price) {
//        this.price = price;
//    }
//
//    public double getPrice() {
//        return this.price;
//    }

    public void printSomething(){
        System.out.println("Method of Potion");
    }
}
