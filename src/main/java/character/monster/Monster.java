package character.monster;

import behavior.Attackable;
import character.AttackType;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Monster {
    @Getter
    @Setter
    private int liftScore;
    @Getter
    @Setter
    private int armor;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int attackPower;
    private Map<AttackType, Attackable> attackType = new HashMap<>();

    public Monster(String name, int liftScore, int armor) {
        this.name = name;
        this.armor = armor;
        this.liftScore = liftScore;
    }

    public Monster(String name, int liftScore, int armor, int attackPower) {
        this.name = name;
        this.armor = armor;
        this.liftScore = liftScore;
        this.attackPower = attackPower;
    }

    public Monster(String name, int liftScore, int armor, int attackPower, Map<AttackType, Attackable> attackType) {
        this.name = name;
        this.armor = armor;
        this.liftScore = liftScore;
        this.attackPower = attackPower;
        this.attackType.putAll(attackType);
    }

    @Override
    public String toString() {
        return "Monster{" +
                "liftScore=" + liftScore +
                ", armor=" + armor +
                ", name='" + name + '\'' +
                ", attackPower=" + attackPower +
                '}';
    }

    public void addTypeAttack(AttackType key, Attackable typeAttack) {
        this.attackType.put(key, typeAttack);
        System.out.println("Add type attack success!!!");
    }

    public void removeTypeAttack(AttackType key){
        System.out.println(this.attackType.remove(key));
        System.out.println("Remove type attack success!!!");
    }

    public void attack(AttackType key){
        Attackable typeAttackUse = this.attackType.get(key);
        if(typeAttackUse != null){
            System.out.print(this.getName());
            typeAttackUse.attack();
        } else{
            System.out.println(this.getName() + " can't attack by " + key);
        }
    }

    public void gotAttack(int damage) {
        if (this.getArmor() >= damage) {
            System.out.println("Defense attack!!");
            return;
        }
        System.out.println("Armor of monster : " + this.getArmor());
        int newLift = this.getLiftScore() - (damage - this.getArmor());
        if (newLift <= 0) {
            this.setLiftScore(0);
            System.out.println("Monster died!!");
            return;
        }
        this.setLiftScore(newLift);
    }

    public int attack() {
        Random rand = new Random();
        int damage = rand.nextInt(this.getAttackPower());
        System.out.println("Damage of Monster : " + damage);
        return damage;
    }
}
