package character.monster;

public class FlyableMonster extends Monster {
    public FlyableMonster(String name, int liftScore, int armor) {
        super(name, liftScore, armor);
    }

    public FlyableMonster(String name, int liftScore, int armor, int attackPower) {
        super(name, liftScore, armor, attackPower);
    }

    public void fly() {
        System.out.println(this.getName() + " กำลังบิน!!!!");
    }
}
