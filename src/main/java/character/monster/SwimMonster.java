package character.monster;

public class SwimMonster extends Monster {
    public SwimMonster(String name, int liftScore, int armor) {
        super(name, liftScore, armor);
    }

    public SwimMonster(String name, int liftScore, int armor, int attackPower) {
        super(name, liftScore, armor, attackPower);
    }

    public void swim() {
        System.out.println(this.getName() + " กำลังว่ายน้ำ!!!");
    }
}
