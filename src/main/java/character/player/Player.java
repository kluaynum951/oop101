package character.player;

import behavior.Attackable;
import character.AttackType;
import item.Backpack;
import item.Item;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Player {
    @Setter
    @Getter
    private int liftScore;
    @Setter
    @Getter
    private int attackScore;
    @Getter
    private String name;
    @Setter
    @Getter
    private Gender gender;
    @Setter
    @Getter
    private int armor;
    private Backpack backpack;
    private Map<AttackType, Attackable> attackType = new HashMap<>();

    public Player(String name) {
        this.name = name;
    }

    public Player(String name, Gender gender, int liftScore) {
        this.name = name;
        this.gender = gender;
        this.liftScore = liftScore;
        this.attackScore = 15;
    }

    public Player(int liftScore) {
        this.liftScore = liftScore;
    }

    public Player(String name, Gender gender, int liftScore, Map<AttackType, Attackable> attackType) {
        this.name = name;
        this.gender = gender;
        this.liftScore = liftScore;
        this.attackScore = 15;
        this.attackType.putAll(attackType);
    }

    @Override
    public String toString() {
        return "Player{" +
                "liftScore=" + liftScore +
                ", attackScore=" + attackScore +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", armor=" + armor +
                '}';
    }

    public void addTypeAttack(AttackType key, Attackable typeAttack) {
        this.attackType.put(key, typeAttack);
        System.out.println("Add type attack success!!!");
    }

    public void removeTypeAttack(AttackType key) {
        System.out.println(this.attackType.remove(key));
        System.out.println("Remove type attack success!!!");
    }

    public void attack(AttackType key) {
        Attackable typeAttackUse = this.attackType.get(key);
        if (typeAttackUse != null) {
            System.out.print(this.getName());
            typeAttackUse.attack();
        } else {
            System.out.println(this.getName() + " can't attack by " + key);
        }
    }

    public void checkTypeAttack() {
        System.out.println(attackType);
    }

    public void createBackpack(int numberOfSlot) {
        backpack = new Backpack(numberOfSlot);
    }

    public void addItemToBackpack(Item item) {
        backpack.addItem(item);
    }

    public void checkBackpack() {
        backpack.printItemList();
    }

    public Item pickItemFromBackpack(int slotIndex) {
        return backpack.pickItem(slotIndex);
    }

    public Item pickItemFromBackpack(Item item) {
        return backpack.pickItem(item);
    }

    public void setName(String newName) {
        if (newName.length() < 10) {
            System.out.println("ชื่อน้อยกว่า 10 ตัวอักษร");
            return;
        }
        this.name = newName;
    }

    public void gotAttack(int damage) {
        if (this.getArmor() >= damage) {
            System.out.println("Defense attack!!");
            return;
        }
        System.out.println("Armor of player : " + this.getArmor());
        int newLift = this.getLiftScore() - (damage - this.getArmor());
        if (newLift <= 0) {
            this.setLiftScore(0);
            System.out.println("Player died!!");
            return;
        }
        this.setLiftScore(newLift);
    }

    public int attack() {
        Random rand = new Random();
        int damage = rand.nextInt(this.getAttackScore());
        System.out.println("Damage of Player : " + damage);
        return damage;
    }

}
